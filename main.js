document.addEventListener("DOMContentLoaded", function () {
    const movieListArray = [
        {
            title: "Avatar",
            releaseYear: 2009,
            duration: 162,
            director: "James Cameron",
            actors: ["Sam Worthington", "Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
            description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
            poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
            rating: 7.9,
        },
        {
            title: "300",
            releaseYear: 2006,
            duration: 117,
            director: "Zack Snyder",
            actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
            description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
            poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
            rating: 7.7,
        },
        {
            title: "The Avengers",
            releaseYear: 2012,
            duration: 143,
            director: "Joss Whedon",
            actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
            description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
            poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
            rating: 8.1,
        },

    ];

    // Récupère les élements du DOM nécessaire
    const container = document.getElementById("container");
    const form = document.getElementById("form");
    const addMovieButton = document.querySelector(".addMovieButton");


    // Récupère la liste de film
    function loadMovieList() {
        const movieListStored = localStorage.getItem("movie_list");

        // Si une liste existe dans LocalStorage, la récupère, sinon utilise la liste initiale
        return movieListStored ? JSON.parse(movieListStored) : movieListArray;
    }

    // Met à jour LocalStorage 
    function updateLocalStorage() {
        localStorage.setItem('movie_list', JSON.stringify(movieList));
    }


    /**AU CHARGEMENT DE LA PAGE**/

    // Charge la liste de films
    const movieList = loadMovieList();

    // Exécute updateList
    updateList();


    /** AFFICHAGE DES FILMS SUR LE DOM**/

    //boucle sur movieList pour afficher les films et les infos qu'elle contient
    function updateList() {

        //vide le contenu du container
        container.innerHTML = "";

        //enregistre movieList dans le stockage --> transformation de l'objet en string pour pouvoir le stocker
        updateLocalStorage()

        //récupère movieList dans le stockage
        const movieListStored = localStorage.getItem("movie_list");

        //tranformation de la string en objet en parsant
        const movies = JSON.parse(movieListStored);

        //parcourt un tableau (ou ici un objet)
        for (const movie of movies) {
            const title = movie.title;
            const releaseYear = movie.releaseYear;
            const duration = movie.duration;
            const director = movie.director;
            const actors = movie.actors;
            const description = movie.description;
            const rating = movie.rating;
            const poster = movie.poster;

            //à chaque itération, créé un article pour le film
            const article = document.createElement("article");

            //créé une balise pour y mettre chaque élément (propriété de l'objet)
            const movieTitle = document.createElement("h3");
            const movieReleaseYear = document.createElement("p");
            const movieDuration = document.createElement("p");
            const movieDirector = document.createElement("p");
            const movieActors = document.createElement("p");
            const movieDescription = document.createElement("p");
            const movieRating = document.createElement("p");
            const moviePoster = document.createElement("img");
            const deleteButton = document.createElement("button");

            //complète le contenu de la propriété
            movieTitle.textContent = title;
            movieReleaseYear.textContent = releaseYear;
            movieDuration.textContent = `Durée : ${duration} min`;
            movieDirector.textContent = `Un film de ${director}`;
            movieActors.textContent = `Avec ${actors}`;
            movieDescription.textContent = description;
            movieRating.textContent = `${rating} / 10`;
            deleteButton.textContent = "Retirer de la liste";

            //ajoute les classes aux éléments
            movieDescription.classList.add("description");
            movieActors.classList.add("actors");
            deleteButton.classList.add("deleteButton");

            //ajoute la source de l'image à afficher
            moviePoster.src = poster;

            //ajoute les éléments à l'article 
            article.appendChild(moviePoster);
            article.appendChild(movieTitle);
            article.appendChild(movieReleaseYear);
            article.appendChild(movieRating);
            article.appendChild(movieDuration);
            article.appendChild(movieDirector);
            article.appendChild(movieActors);
            article.appendChild(movieDescription);
            article.appendChild(deleteButton);

            //ajoute l'article à #container 
            container.appendChild(article);


            /**SUPPRESSION FILM **/

            //écoute le clic sur le bouton deleteButton. Exécute deleteMovie au clic.
            deleteButton.addEventListener('click', deleteMovie);

            //supprime un film
            function deleteMovie(event) {
                //Récupère le bouton cliqué
                const deleteButtonClicked = event.target;

                //Récupère l'article parent du bouton concerné
                const articleToDelete = deleteButtonClicked.parentNode;

                //Récupère le titre de l'article à supprimer
                const articleToDeleteTitle = article.querySelector("h3").textContent;

                //Supprime l'article du DOM
                container.removeChild(articleToDelete);



                //compare le titre de l'article à supprimer avec le titre de chaque article
                function compareTitle(article) {
                    return article.title === articleToDeleteTitle;
                }

                //parcourt le tableau movieList et retourne la valeur de l'index lorsque la fonction compareTitle retourne "true"
                const indexToDelete = movieList.findIndex(compareTitle);

                //supprime l'index du tableau movieList
                movieList.splice(indexToDelete, 1);

                //met à jour LocalStorage
                updateLocalStorage();

            }

        }


    }

    /**FORMULAIRE POUR AJOUTER UN FILM**/

    //écoute le clic sur le bouton pour afficher le formulaire ou le cacher
    addMovieButton.addEventListener('click', toggleForm);

    //affiche ou cache le formulaire
    function toggleForm() {
        if (form.classList.contains("hidden")) {
            // Si le formulaire est caché, l'afficher
            form.classList.remove("hidden");
            addMovieButton.textContent = "Fermer";
        } else {
            // Sinon, le cacher
            form.classList.add("hidden");
            addMovieButton.textContent = "Ajouter un film";
        }
    }


    //écoute la soumission du formulaire pour ajouter un film. Exécute addMovie à la soumission.
    form.addEventListener('submit', addMovie);

    //ajoute un film à la liste
    function addMovie(event) {
        event.preventDefault();

        //récupère les value des inputs du formulaire
        const newTitle = document.getElementById("newTitle").value;
        const newDuration = document.getElementById("newDuration").value;
        const newDirector = document.getElementById("newDirector").value;
        const newActors = document.getElementById("newActors").value;
        const newDescription = document.getElementById("newDescription").value;
        const newRating = document.getElementById("newRating").value;
        const newPoster = document.getElementById("newPoster").value;

        // Créé un nouvel objet avec les données récupérées du formulaire
        const newMovie = {
            title: newTitle,
            duration: newDuration,
            director: newDirector,
            actors: newActors,
            description: newDescription,
            rating: newRating,
            poster: newPoster,
        };

        // Ajoute le nouvel objet à movieList
        movieList.push(newMovie);

        //met à jour LocalStorage
        updateLocalStorage()

        //exécute la fonction qui permet de boucler sur movieList
        updateList()

        //Cache le formulaire
        form.classList.add("hidden");

        //remodifie le texte du bouton
        addMovieButton.textContent = "Ajouter un film";

        //réinitialise les champs du formulaire
        form.reset();

    }

});


//ajouter tri par ordre alphabétique, par année, par note

/* données test pour l'ajout d'un film: 
Film : Joker
Durée : 122
Réal : Todd Phillips
Acteurs :  Joaquin Phoenix, Robert De Niro, Zazie Beetz
Description :  Le film, qui relate une histoire originale inédite sur grand écran, se focalise sur la figure emblématique de l’ennemi juré de Batman. Il brosse le portrait d’Arthur Fleck, un homme sans concession méprisé par la société. 
Note : 8.5
URL: https://fr.web.img6.acsta.net/pictures/19/09/03/12/02/4765874.jpg
*/